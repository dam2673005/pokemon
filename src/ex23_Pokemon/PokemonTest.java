package ex23_Pokemon;

import java.util.List;
import java.util.Scanner;

public class PokemonTest {
    public static void main(String[] args) {
        PokemonManager manager = new PokemonManager();
        Scanner scanner = new Scanner(System.in);

        int option = -1;

        while (option != 0) {
            System.out.println("MENU");
            System.out.println("1. TOTALS");
            System.out.println("2. POKEMON SEARCH");
            System.out.println("3. EXTRACT GENERATION");
            System.out.println("4. ORDER LEGENDARY");
            System.out.println("5. ORDER BY TYPE1 AND TYPE2");
            System.out.println("6. ORDER BY GENERATION AND NAME");
            System.out.println("0. QUIT");
            System.out.print("CHOOSE MENU OPTION: ");

            try {
                option = Integer.parseInt(scanner.nextLine());
                switch (option) {
                    case 1:
                        // Test readTotals
                        System.out.println("Enter Type1 (ENTER for all): ");
                        String type1 = scanner.nextLine();
                        System.out.println("Enter Type2 (ENTER for all): ");
                        String type2 = scanner.nextLine();
                        String totals = manager.readTotals("pokemons.csv", type1, type2);
                        System.out.println(totals);
                        break;
                    case 2:
                        // Test searchPokemons
                        System.out.println("Enter Pokemon name to search: ");
                        String name = scanner.nextLine();
                        List<Pokemon> foundPokemons = manager.searchPokemons("pokemons.csv", name);
                        for (Pokemon pokemon : foundPokemons) {
                            System.out.println(pokemon);
                        }
                        break;
                    case 3:
                        // Test extractGeneration
                        System.out.println("Enter Generation: ");
                        byte generation = Byte.parseByte(scanner.nextLine());
                        List<Pokemon> extractedPokemons = manager.extractGeneration("pokemons.csv", generation);
                        for (Pokemon pokemon : extractedPokemons) {
                            System.out.println(pokemon);
                        }
                        break;
                    case 4:
                        // Test orderLegendary
                        System.out.println("Enter Legendary (1. TRUE, 2. FALSE): ");
                        byte legendary = Byte.parseByte(scanner.nextLine());
                        List<Pokemon> orderedPokemons = manager.orderLegendary("pokemons.csv", legendary);
                        for (Pokemon pokemon : orderedPokemons) {
                            System.out.println(pokemon);
                        }
                        break;
                    case 5:
                        // Test orderType
                        List<Pokemon> orderedByType = manager.orderType("pokemons.csv");
                        for (Pokemon pokemon : orderedByType) {
                            System.out.println(pokemon);
                        }
                        break;
                    case 6:
                        // Test orderGenerationName
                        List<Pokemon> orderedByGenerationName = manager.orderGenerationName("pokemons.csv");
                        for (Pokemon pokemon : orderedByGenerationName) {
                            System.out.println(pokemon);
                        }
                        break;
                    case 0:
                        System.out.println("Exiting...");
                        break;
                    default:
                        System.out.println("Invalid option. Please choose a valid option.");
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number.");
            }
        }

        scanner.close();
    }
}
