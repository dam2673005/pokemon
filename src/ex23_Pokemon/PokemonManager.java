package ex23_Pokemon;

import java.io.*;
import java.util.*;

public class PokemonManager {
    
    public String readTotals(String fileInput, String type1, String type2) {
        try (Scanner scanner = new Scanner(new File(fileInput))) {
            StringBuilder result = new StringBuilder();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(";");
                if ((type1.isEmpty() || parts[2].equalsIgnoreCase(type1))
                        && (type2.isEmpty() || parts[3].equalsIgnoreCase(type2))) {
                    int total = Integer.parseInt(parts[4]) + Integer.parseInt(parts[5])
                            + Integer.parseInt(parts[6]) + Integer.parseInt(parts[7])
                            + Integer.parseInt(parts[8]) + Integer.parseInt(parts[9]);
                    result.append(line).append(";").append(total).append("\n");
                }
            }
            return result.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void write(String text, String fileOutput) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(fileOutput))) {
            writer.write(text);
            System.out.println("FILE " + fileOutput + " CREATED.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Pokemon> searchPokemons(String fileInput, String pokemonName) {
        List<Pokemon> foundPokemons = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileInput))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains(pokemonName)) {
                    String[] parts = line.split(";");
                    foundPokemons.add(new Pokemon(
                            Integer.parseInt(parts[0]), // code
                            parts[1], // name
                            parts[2], // type1
                            parts[3], // type2
                            Integer.parseInt(parts[4]), // healthPoints
                            Integer.parseInt(parts[5]), // attack
                            Integer.parseInt(parts[6]), // defense
                            Integer.parseInt(parts[7]), // specialAttack
                            Integer.parseInt(parts[8]), // specialDefense
                            Integer.parseInt(parts[9]), // speed
                            Byte.parseByte(parts[10]), // generation
                            Byte.parseByte(parts[11]) == 1 // legendary
                    ));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return foundPokemons;
    }

    public List<Pokemon> extractGeneration(String fileInput, byte generation) {
        List<Pokemon> extractedPokemons = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileInput))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(";");
                if (Byte.parseByte(parts[10]) == generation) {
                    extractedPokemons.add(new Pokemon(
                            Integer.parseInt(parts[0]), // code
                            parts[1], // name
                            parts[2], // type1
                            parts[3], // type2
                            Integer.parseInt(parts[4]), // healthPoints
                            Integer.parseInt(parts[5]), // attack
                            Integer.parseInt(parts[6]), // defense
                            Integer.parseInt(parts[7]), // specialAttack
                            Integer.parseInt(parts[8]), // specialDefense
                            Integer.parseInt(parts[9]), // speed
                            Byte.parseByte(parts[10]), // generation
                            Byte.parseByte(parts[11]) == 1 // legendary
                    ));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return extractedPokemons;
    }

    public List<Pokemon> orderLegendary(String fileInput, byte legendary) {
        List<Pokemon> orderedPokemons = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileInput))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(";");
                if (Byte.parseByte(parts[11]) == legendary) {
                    orderedPokemons.add(new Pokemon(
                            Integer.parseInt(parts[0]), // code
                            parts[1], // name
                            parts[2], // type1
                            parts[3], // type2
                            Integer.parseInt(parts[4]), // healthPoints
                            Integer.parseInt(parts[5]), // attack
                            Integer.parseInt(parts[6]), // defense
                            Integer.parseInt(parts[7]), // specialAttack
                            Integer.parseInt(parts[8]), // specialDefense
                            Integer.parseInt(parts[9]), // speed
                            Byte.parseByte(parts[10]), // generation
                            Byte.parseByte(parts[11]) == 1 // legendary
                    ));
                }
            }
            Collections.sort(orderedPokemons);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return orderedPokemons;
    }

    public List<Pokemon> orderType(String fileInput) {
        List<Pokemon> orderedByType = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileInput))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(";");
                orderedByType.add(new Pokemon(
                        Integer.parseInt(parts[0]), // code
                        parts[1], // name
                        parts[2], // type1
                        parts[3], // type2
                        Integer.parseInt(parts[4]), // healthPoints
                        Integer.parseInt(parts[5]), // attack
                        Integer.parseInt(parts[6]), // defense
                        Integer.parseInt(parts[7]), // specialAttack
                        Integer.parseInt(parts[8]), // specialDefense
                        Integer.parseInt(parts[9]), // speed
                        Byte.parseByte(parts[10]), // generation
                        Byte.parseByte(parts[11]) == 1 // legendary
                ));
            }
            Comparator<Pokemon> comparator = new ComparatorType();
            Collections.sort(orderedByType, comparator);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return orderedByType;
    }

    public List<Pokemon> orderGenerationName(String fileInput) {
        List<Pokemon> orderedByGenerationName = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileInput))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(";");
                orderedByGenerationName.add(new Pokemon(
                        Integer.parseInt(parts[0]), // code
                        parts[1], // name
                        parts[2], // type1
                        parts[3], // type2
                        Integer.parseInt(parts[4]), // healthPoints
                        Integer.parseInt(parts[5]), // attack
                        Integer.parseInt(parts[6]), // defense
                        Integer.parseInt(parts[7]), // specialAttack
                        Integer.parseInt(parts[8]), // specialDefense
                        Integer.parseInt(parts[9]), // speed
                        Byte.parseByte(parts[10]), // generation
                        Byte.parseByte(parts[11]) == 1 // legendary
                ));
            }
            Comparator<Pokemon> comparator = new ComparatorGenerationName();
            Collections.sort(orderedByGenerationName, comparator);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return orderedByGenerationName;
    }
}