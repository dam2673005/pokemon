package ex23_Pokemon;

import java.util.Comparator;

public class ComparatorType implements Comparator<Pokemon> {
    @Override
    public int compare(Pokemon p1, Pokemon p2) {
        int result = p1.getType1().compareTo(p2.getType1());
        if (result == 0) {
            return p1.getType2().compareTo(p2.getType2());
        }
        return result;
    }
}