package ex23_Pokemon;

import java.util.Comparator;

public class ComparatorGenerationName implements Comparator<Pokemon> {
    @Override
    public int compare(Pokemon p1, Pokemon p2) {
        int result = Byte.compare(p1.getGeneration(), p2.getGeneration());
        if (result == 0) {
            return p1.getName().compareTo(p2.getName());
        }
        return result;
    }
}